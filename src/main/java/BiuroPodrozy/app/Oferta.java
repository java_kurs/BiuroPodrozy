package BiuroPodrozy.app;

import org.joda.time.LocalDate;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by T420 on 2017-07-21.
 */
public class Oferta {

    enum Platnosc{
        KARTA,
        GOTOWKA,
        PRZELEW
    }
    enum Rezerwacja{
        NA_MIEJSCU,
        ON_LINE
    }

    private List<Wycieczka> listaWycieczek;
    private  Platnosc platnosc;
    private  Rezerwacja rezerwacja;

    public Oferta(){
        listaWycieczek = new ArrayList<Wycieczka>();
    }

    public void addWycieczka(Wycieczka wycieczka){
        listaWycieczek.add(wycieczka);
    }

    public void deleteWycieczka(String place,int year,int month,int day){
        LocalDate date =new LocalDate(year,month,day);
        int size = listaWycieczek.size();
        Wycieczka wyTemp;
        for (int i = 0; i <size ; i++) {
            wyTemp = listaWycieczek.get(i);
            if (wyTemp.getCzasPrzyjazdu().equals(date) && wyTemp.getMiejscePobytu().equals(place)){
                listaWycieczek.remove(i);
                size=listaWycieczek.size();
            }
        }
    }

    public List<Wycieczka> getListaWycieczek() {
        return listaWycieczek;
    }
}

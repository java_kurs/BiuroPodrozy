package BiuroPodrozy.app;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Created by T420 on 2017-07-21.
 */
public class BiuroPodrozy {

    private Oferta oferta;
    private List<Wycieczka> listaTemp;

    private String sortowanie,zakres,rPlatnosci;
    private int rodzaj;

    public BiuroPodrozy(Oferta oferta) {
        this.oferta = oferta;
    }

    public void initList() {
        listaTemp = new ArrayList<Wycieczka>(oferta.getListaWycieczek());
    }

    public void uI() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj rodzaj sortowania (cena/alfabetycznie)");
        this.sortowanie = scanner.next();
        System.out.println("Wybierz kryterium dodatkowe - rodzaje wycieczki : " +
                "\nobjazdowa(1)/wypoczynek(2)/zwiedzanie(3)/wszystkie(4)");
        int rodzajWycieczki = scanner.nextInt();
        System.out.println("Podaj zakres cenowy/ ew wybierz 0");
        this.zakres = scanner.next();
        System.out.println("Czy chcesz wybrac rodzaj platnosci t/n");
        this.rPlatnosci = scanner.nextLine().substring(0,1);
    }
}

package BiuroPodrozy.app;

import org.joda.time.LocalDate;

/**
 * Created by T420 on 2017-07-21.
 */
public class Wycieczka {
    enum TypWycieczki {
        OBJAZDOWA,
        WYPOCZYNEK,
        ZWIEDZANIE
    }

    enum Gwiazdki {
        JEDNA, DWIE, TRZY, CZTERY, PIEC
    }

    private String miejscePobytu;
    private LocalDate czasPrzyjazdu;
    private LocalDate czasOdjazdu;
    private int dlugoscPobytu;
    private TypWycieczki typ;
    private Gwiazdki gwiazdki;

    public Wycieczka() {
    }

    @Override
    public String toString() {
        return "Wycieczka{" +
                "miejscePobytu='" + miejscePobytu + '\'' +
                ", czasPrzyjazdu=" + czasPrzyjazdu +
                ", czasOdjazdu=" + czasOdjazdu +
                ", dlugoscPobytu=" + dlugoscPobytu +
                ", typ=" + typ +
                ", gwiazdki=" + gwiazdki +
                '}';
    }

    private Wycieczka(WyczieczkaBuilder builder) {

        this.czasPrzyjazdu = new LocalDate(builder.year, builder.month, builder.day);
        this.dlugoscPobytu = builder.dlugosc;
        this.czasOdjazdu = calcCzasOdjazdu();
        this.typ = builder.typ;
        this.gwiazdki = builder.gwiazdki;
        this.miejscePobytu=builder.miejsce;
    }

    public LocalDate calcCzasOdjazdu() {
        return czasPrzyjazdu.plusDays(dlugoscPobytu);
    }


    public LocalDate getCzasPrzyjazdu() {
        return czasPrzyjazdu;
    }

    public void setCzasPrzyjazdu(LocalDate czasPrzyjazdu) {
        this.czasPrzyjazdu = czasPrzyjazdu;
    }

    public LocalDate getCzasOdjazdu() {
        return czasOdjazdu;
    }

    public void setCzasOdjazdu(LocalDate czasOdjazdu) {
        this.czasOdjazdu = czasOdjazdu;
    }

    public int getDlugoscPobytu() {
        return dlugoscPobytu;
    }

    public void setDlugoscPobytu(int dlugoscPobytu) {
        this.dlugoscPobytu = dlugoscPobytu;
    }

    public String getMiejscePobytu() {
        return miejscePobytu;
    }

    public static class WyczieczkaBuilder {
        private int year, month, day, dlugosc;
        private TypWycieczki typ;
        private Gwiazdki gwiazdki;
        private String miejsce;

        public WyczieczkaBuilder(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public WyczieczkaBuilder dlugoscPobytu(int dlugosc) {
            this.dlugosc = dlugosc;
            return this;
        }

        public WyczieczkaBuilder typWycieczki(TypWycieczki typ) {
            this.typ = typ;
            return this;
        }

        public WyczieczkaBuilder iloscGwiazdek(Gwiazdki gwiazdki) {
            this.gwiazdki = gwiazdki;
            return this;
        }

        public WyczieczkaBuilder miejscePobytu(String miejsce){
            this.miejsce = miejsce;
            return this;
        }

        public Wycieczka build() {
            return new Wycieczka(this);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Wycieczka wycieczka = (Wycieczka) o;

        if (dlugoscPobytu != wycieczka.dlugoscPobytu) return false;
        if (!miejscePobytu.equals(wycieczka.miejscePobytu)) return false;
        if (!czasPrzyjazdu.equals(wycieczka.czasPrzyjazdu)) return false;
        if (!czasOdjazdu.equals(wycieczka.czasOdjazdu)) return false;
        if (typ != wycieczka.typ) return false;
        return gwiazdki == wycieczka.gwiazdki;
    }

    @Override
    public int hashCode() {
        int result = miejscePobytu.hashCode();
        result = 31 * result + czasPrzyjazdu.hashCode();
        result = 31 * result + czasOdjazdu.hashCode();
        result = 31 * result + dlugoscPobytu;
        result = 31 * result + typ.hashCode();
        result = 31 * result + gwiazdki.hashCode();
        return result;
    }
}

package BiuroPodrozy.app;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by T420 on 2017-07-21.
 */

@RunWith(MockitoJUnitRunner.class)
public class WycieczkaTest {

    @Spy
    Wycieczka wycieczka = new Wycieczka();


    @Test
    public void calcCzasOdjazduTest(){
        LocalDate czasPrzyjazdu = new LocalDate(2017,7,21);
        LocalDate czasTest = new LocalDate(2017,7,28);
        int czasPobytu = 7;

        assertThat(czasPrzyjazdu.plusDays(czasPobytu)).isEqualTo(czasTest);


    }


}
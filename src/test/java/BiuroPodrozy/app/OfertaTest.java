package BiuroPodrozy.app;

import org.joda.time.LocalDate;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by T420 on 2017-07-21.
 */
@RunWith(MockitoJUnitRunner.class)
public class OfertaTest {

    @Spy
    Oferta oferta = new Oferta();

    Wycieczka wycieczka;
    Wycieczka wycieczka2;

    @Before
    public void setup() {
        wycieczka = new Wycieczka.WyczieczkaBuilder(2017, 7, 21).miejscePobytu("Majorka").build();
        wycieczka2 = new Wycieczka.WyczieczkaBuilder(2017, 7, 21).miejscePobytu("Kreta").build();
        oferta.addWycieczka(wycieczka);
        oferta.addWycieczka(wycieczka2);
    }
    /*
    @Test

    public void addWycieczkaTest() throws Exception {
        oferta.getListaWycieczek().add(wycieczka);

        assertThat(oferta.getListaWycieczek().get(0)).isEqualTo(wycieczka);
    }*/

    @Test
    public void deleteWycieczka() {
        LocalDate date = new LocalDate(2017, 7, 21);
        String place = "Majorka";
        int size = oferta.getListaWycieczek().size();
        Wycieczka wyTemp;
        for (int i = 0; i < size; i++) {
            wyTemp = oferta.getListaWycieczek().get(i);
            if (wyTemp.getCzasPrzyjazdu().equals(date) && wyTemp.getMiejscePobytu().equals(place)) {
                oferta.getListaWycieczek().remove(i);
                size = oferta.getListaWycieczek().size();
            }

        }
        assertThat(oferta.getListaWycieczek().get(0)).isEqualTo(wycieczka2);

    }

}